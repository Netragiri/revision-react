import React, { useState } from 'react'
import ChildComp from './ChildComp'

function ParentComp() {
    const [name,setName]=useState('')
    function handler(childdata){
        setName(childdata)
    }
  return (
    <div>
      <ChildComp func={handler} />
      {name}
    </div>
  )
}

export default ParentComp
