import React from 'react'

function ArrayextraTask() {

    // const array = [
    //     {
    //         name: 'Akshat',
    //         age: 20
    //     },
    //     {
    //         name: 'Keval',
    //         age: 25
    //     }
    // ]

    // return (
    //     <div >
    //         {
    //             array.map((item) => { return <h1 key={item.age}> {item.name}</h1> })
    //         }
    //     </div>

    // )

    // const array=[12,3,1,4,23,12,34]
    // return (
    //         <div >
    //             {/* //returns only first element */}
    //                 { array.find((item) => { return item>18 })}
    //                 {/* returns all elements which fullfiled by condition */}
    //                {array.filter((item) => { return item>18 })} 

    //         </div>
    // )


    const array = ['Akshat', 23, 12, 'keval', 233, 12, 45, 2, 100, 231, 34, 26, 5, 7, 54, 23, 'zipal', 'angular']
    // const copy = [...array]

    return (
        <h1>
            SortedArray:
            {

                [array.filter(
                    values => !isNaN(values)).sort((a, b) => a - b).join(','),
                 array.filter(
                     values => isNaN(values)).sort().join(',')].join(',')

                // array.filter((values)=>
                //     !isNaN(values)
                //     ? values.sort((a, b) => a - b).join(',') : values.sort().join(','))

                // //     array.sort((a,b) => !isNaN(a) ? a-b : null)
            }
        </h1>
    )
}

export default ArrayextraTask
