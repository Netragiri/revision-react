import React from 'react'



//this is function component in another function component without making new file
function FunctionComp() {
    function ChildComp(){
        return <h1>This is child component of function component </h1>
    }
  return (
    <div>
      <ChildComp />
    </div>
  )
}

export default FunctionComp
