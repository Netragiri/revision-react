import logo from './logo.svg';
import './App.css';
import WithoutJsx from './WithoutJsx'
import FunctionComp from './FunctionComp';
import ClassComp from './ClassComp';
import ClickHOC from './ClickHOC';
import HoverHOC from './HoverHOC';
import PureComp from './PureComponent';
import StateClassComp from './StateClassComp';
import ArrayMethods from './ArrayMethods';
import ArrayextraTask from './ArrayextraTask';
import PropsFunctionComp from './PropsFunctionComp';
import { createContext, useState } from 'react';
import ParentComp from './ParentComp';
import AsyncAwait from './AsyncAwait';
import UseMemo from './UseMemo';
import UseRef from './UseRef';
import Lifecycle from './Lifecycle'
import FuncContextApi from './FuncContextApi';
import ParentForwardref from './ParentForwardref';
import FormikForm from './FormikForm';
import HookForm from './HookForm';


const ThemeContext=createContext()

function App() {
    const [name,setName]=useState('Anil')
  function hello(){
      return setName('Hello')
  }
 
 return (
  <>
  {/* <WithoutJsx />  */}
  {/* <FunctionComp />
  <ClassComp /> */}
  {/* <ClickHOC />
  <HoverHOC /> */}
  {/* <PureComp /> */}
  {/* <StateClassComp /> */}
  {/* <ArrayMethods /> */}
  {/* <ArrayextraTask /> */}
  {/* <PropsFunctionComp name={name} func={hello}/> */}
  
  {/* for pass data from child to parent */}
  {/* <ParentComp /> */}
  {/* <AsyncAwait /> */}
  {/* <UseMemo />
  <UseRef />
  <Lifecycle /> */}
  {/* <ThemeContext.Provider value='Bob'>
  <FuncContextApi />
  </ThemeContext.Provider> */}
  {/* <ParentForwardref /> */}
 {/* <FormikForm /> */}
 <HookForm />


  </>
 )
}
export {ThemeContext};
export default App;

