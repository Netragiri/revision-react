import React, { Component } from 'react'


//this is class component in another class component without making new file
 class ClassComp extends Component {
  render() {

    class ChildComp extends Component{
        render(){
            return <h2>This is child component  of class component</h2>
        }
    }


    return (
      <div>
        <ChildComp />
      </div>
    )
  }
}

export default ClassComp
