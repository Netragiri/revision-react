import React, { Component } from 'react'

export class Lifecycle extends Component {
    constructor(props) {
        console.log('constuctor')
      super(props)
    
      this.state = {
         count:0
      }
    }
    
    shouldComponentUpdate(){
        console.log('shouldcomponentupdate')
        return true
    }
    componentDidUpdate(){
        console.log('componentdidupdate')
    }
  render() {
      console.log('render')
    return (
      <div>
          <h1>{this.state.count}</h1>
        <button onClick={()=>this.setState({count:this.state.count+1})}>increment</button>
      </div>
    )
  }
}

export default Lifecycle
