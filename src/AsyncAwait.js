import React from 'react'

function AsyncAwait() {



    async function Demo(){
        let promise=new Promise((resolve,reject)=>{
            let a=3;
            if(a==3)
            setTimeout(()=>{resolve('Netra async-await')},2000);
        })
        const abc=await promise;
        console.log(abc)

    }
    Demo();
  return (
    <div>
      <h1>Demo</h1>
    </div>
  )
}

export default AsyncAwait
