import React from 'react'
import { useForm } from 'react-hook-form'

function HookForm() {
    const{register,handleSubmit}=useForm()
    const submithandler=()=>{
        console.log(object)
    }
  return (
    <div>
      <form onSubmit={handleSubmit(submithandler)}>
          <input type='text' name='text' ref={register}></input>
          <input type='file' name='file'></input>
      </form>
    </div>
  )
}

export default HookForm
