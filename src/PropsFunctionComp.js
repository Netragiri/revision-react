import React from 'react'

function PropsFunctionComp(props) {
  return (
    <div>
      <h1>{props.name}</h1>
      <button onClick={props.func}>click me</button>
    </div>
  )
}

export default PropsFunctionComp
