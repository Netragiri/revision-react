import React from 'react'

//h1 and h2 in div without jsx
function WithoutJsx() {
  return (
    React.createElement('div',null,React.createElement('h1',null,'this is h1'),React.createElement('h2',null,'this is h2'))
  )
}

export default WithoutJsx
