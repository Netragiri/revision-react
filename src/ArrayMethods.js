import React from 'react'

function ArrayMethods() {

    //map function array
    function Map(){
        let array=[23,45,23,98,65];
        let maparray=array.map((item)=>item*2)
        console.log('map function=======>')
        console.log(array)
        return(
         <>
         <h1>Map function Array</h1>
         <h4>old array : {array.join(',')}</h4>
           <h4>
              mapped array : {maparray.join(',')}
            </h4> 
        </>
        )
        
    }


    //filter array
    function Filter(){
        let array=[34,56,23,98,1,3,5,9];
        let filterArray=array.filter(n=>n>23)
        console.log('Filter method=======>')
        console.log(filterArray)

        return (
            <>
            <h1>Filter method</h1>
            <h4>old array : {array.join(',')}</h4>
            <h4>filter array : {filterArray.join(',')}</h4>
            </>
        )
    }


    //sort array
    function Sort(){
        let array=[1,5,7,10,23,34,8,'A','c','Z','u',15,767,23];
        let sortArray=array.sort((a,b)=>a-b)
        console.log(sortArray)
        return(
            <>
                <h1>Sort method</h1>
                <h4>sorted array : {sortArray.join(',')}</h4>
            </>
        )
    }

    //for each method
    function ForEach(){
        let array=[1,2,3]
       
        return(
            <>
                <h4>{array.forEach((item)=>{
                    console.log('Foreach method=====>')
                    console.log(item)
                })}</h4>
            </>
        )
    }


    //function concat
    function Concat(){
        let array1=['a','b','c',]
        let array2=['d','e','f',]
        return (
            <>
            <h1>Concat Array</h1>
            <h4>{array1.concat(array2).join(',')}</h4>
            </>
        )
    }

    //Every method
    function Every(){
        let array=[1,2,3,4]
        let array2=['abc','avbcd','bbvcbxvcx']
        let every_item=array.every((item)=>item>2);
        let every_item2=array2.every((item)=>item[0].startsWith('a'));
        console.log('Every method======>')
        console.log(every_item)
        console.log(every_item2)
        return (
            <>
            </>
        )
    }

     //Some method
     function Some(){
        let array=[1,2,3,4]
        let some_item=array.some((item)=>item>2);
        console.log('Some method======>')
        console.log(some_item)
        return (
            <>
            </>
        )
    }

      //Includes method
      function Includes(){
        let array=[1,2,3,4]
        let Includes_item=array.includes(9);
        console.log('Includes method======>')
        console.log(Includes_item)
        return (
            <>
            </>
        )
    }

    //reduce method
    function Reduce(){
        let array=[1,2,3,4,]
        return(
            <>
            <h1>Reduce method</h1>
            <h4>array : {array.join(',')}</h4>
            <h4>abstraction of array elememt : {array.reduce((total,sum)=>{
               return  sum+total;

            })}</h4>
            </>
        )
    }

    //find ,ethod
    //it check condition and return first elememt which satisfy the condition 
    //if no matches found it return -1
    function Find(){
        let array=['pqr','abc','x','ty']
        return (
            <>
            <h4>{array.find((item)=>item.length==2)}</h4>
            </>
        )
    }
    //find index method
    function FindIndex(){
        let array=[1,5,7,9,3,89]
        return (
            <>
                <h1>Find index</h1>
                <h4>main array : {array.join(',')}</h4>
                <h4>index of 7 is :{array.findIndex((item)=>{
                    return item==7
                })}</h4>
            </>
        )
    }

    //index of method
    function IndexOf(){
        let array=['ghanshyam','manan','aman','ramesh']
        return(
            <>
            <h1>Index of method</h1>
            <h4>original array : {array.join(',')}</h4>
            <h4>index of manan{array.indexOf('manan')}</h4>
            </>
        )
        
    }
    //fill method fill value in every index of array
    function Fill(){
        let array=new Array(5)
        return (
            <>
                <h1>Fill method</h1>
                <h4>{console.log(array.fill(10)) }</h4>
            </>
        )
    }

    //Slice method make slice of given array
    function Slice(){
        let array=[1,2,3,4,5,6]
        return(
            <>
            <h1>Slice method</h1>
            <h4>Old array : {array.join(',')}</h4>
            <h4> sliced array : {array.slice(2,5).join(',')}</h4>
            </>
        )
    }


    //return element that is remove at first index
    function Shift(){
        let array=['apple','banana','orange','guavava','kiwi']
        return(
            <>
            <h1>Shift method</h1>
            <h4>Old array : {array.join(',')}</h4>
            <h4> removed element : {array.shift()}</h4>
            </>
        )
    }


//add elememnt in first element and return new length of array
    function Unshift(){
        let array=['apple','banana','orange','guavava','kiwi']
        return(
            <>
            <h1>UnShift method</h1>
            <h4>Old array : {array.join(',')}</h4>
            <h4> total indexes in new array  : {array.unshift('watermalon')}</h4>
            <h4>after unshifting new array : {array.join(',')}</h4>
            </>
        )
    }
    //Splice method
    //here add elements at 3rd index and and remove 2 elements from 3rd index
    //splice method return deleted elements
    function Splice (){
        let array=['apple','banana','orange','guavava','kiwi']
        return (
            <>
                <h1>Splice method</h1>
                <h4>{array.splice(3,2,'mango','muskmalon','blueberry').join(',')}</h4>
                <h4>{array.join(',')}</h4>
            </>
        )
    }


    //convert string into array
    function From(){
        return (
            <>
            <h1>From method</h1>
            <h4>{Array.from('hello').join(',')}</h4>
            </>
        )
    }

//make array of single element
    function ArrayOf(){
        let array
        return (
            <>
            <h1>Arrayof method</h1>
            <h4>{array=Array.of(42)}</h4>
            {console.log(array)}
            </>
        )
    }

    //copywithin method
    //to copy elemnt from same array in same array 
    //here i want to copy 1,2 at index 3
    //so target index is 3
    //start copy index is 0 for value 1 and end index should be 2 because it takes 0and 1 index
    //(target ,starting,ending  )
    function CopyWithIn(){
        let array=[1,2,3,4,5,6,7,8,9,10]
        return (
            <>
            <h1>CopyWithIn method</h1>
            <h4>{array.copyWithin(3,0,2).join(',')}</h4>
            </>
        )
    }


    //entery return key and valur
    function Entries(){
        let array=['apple','banana','orange','guavava','kiwi']
        
        return(
            <>
            <h1>Entries</h1>
                <h4>
                {console.log('Entries array======>')}
                    {console.log(...array.entries())}
                </h4>
                <h4>
                    {console.log('key array======>')}
                    {console.log(...array.keys())}
                    
                </h4>
                <h4>
                {console.log('value array======>')}
                    {console.log(...array.values())}
                </h4>
            </>
        )
    }
  return (
    <div>
      <Map />
      <Filter />
      <Sort />
      <ForEach />
      <Concat />
      <Every />
      <Some />
      <Includes />
      <Reduce />
      <Find />
      <FindIndex />
      <IndexOf />
      <Fill />
      <Slice />
      <Shift />
      <Unshift />
      <Splice />
      <From />
      <ArrayOf />
      <CopyWithIn />
      <Entries />
    </div>
  )
}

export default ArrayMethods
