import React, { useState } from 'react'
import UpdatedComponenet from './Hoc'

function HoverHOC(props) {
    
  return (
    <div>
      <h2 onMouseOver={props.incrementCount}>Hover {props.item} times</h2>
    </div>
  )
}

export default UpdatedComponenet(HoverHOC)
