import React, { useCallback, useMemo, useState } from 'react'

function UseMemo() {
    const[name,setName]=useState(10)
    const[count,setCount]=useState(0)
    // const clickhandler=()=>{
    //     console.log('function call')
    //     setName('Allice')

    // }
    const changenumber=React.useMemo(
        ()=> {
          console.warn("Hello")
          return name;
        }
      ,[name])
  return (
    <>
        <h1>usememo</h1>
        <h4>{changenumber}</h4>
      <button onClick={()=>{setName(name*10)}}>ClickMe</button>
      <button onClick={()=>{setCount(count+1)}}>change count</button>
    </>
  )
}

export default UseMemo
