import React, { useContext } from 'react'
import { ThemeContext } from './App'

function FuncContextApi() {
    const user=useContext(ThemeContext)
  return (
    <div>
      <h1>hello {user}</h1>
    </div>
  )
}

export default FuncContextApi
