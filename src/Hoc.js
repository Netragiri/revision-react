import React,{useState} from "react";


const UpdatedComponenet=(OriginalComponent)=>{
    function NewComponent(){
        const [count,setCount]=useState(0)

        const incrementCount=()=>{
            setCount(count+1)
        }
        return(
            <>
            <OriginalComponent item={count} incrementCount={incrementCount} /></>
        )
    }
    return NewComponent 
}
export default UpdatedComponenet