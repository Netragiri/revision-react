import React from 'react'

function ChildComp(props) {
    const submithandler=(e)=>{
        props.func(e.target.myname.value)
        e.preventDefault()
    }
  return (
    <div>
      <form onSubmit={submithandler}>
          <input type='text' placeholder='enter name' name='myname'></input>
          <input type='submit'></input>
      </form>
    </div>
  )
}

export default ChildComp
