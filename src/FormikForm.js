import React from 'react'
import { Formik,Form,Field,ErrorMessage } from 'formik'
import * as yup from 'yup';

const FormikForm=()=> {
const validation=yup.object().shape({
  firstname:yup.
            string().
            min(5).
            required(),
  lastname:yup.
            string().
            required(),
  address:yup.
          string().
          max(150).
          required(),
  number:yup.
          number().
          required(),
  email:yup.
          string().
          email().
          required(),
  password:yup.
            string().
            matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,'At least contain one letter one number and one special character').
            min(8,'password is too short').
            max(20). 
            required(),
  confirm_password:yup.
            string().
            oneOf([yup.ref('password')], 'Your passwords do not match.').
            required()
   
          })  

const Error=({name})=>{
  return(
    <>
    <div style={{color:'red'}}>
      <ErrorMessage name={name} />
      </div>
      </>
  )
}



  return (
    <div>
      <Formik
        validationSchema={validation} 
       initialValues={{
              firstname:'',
              lastname:'',
              address:'',
              number:'',
              email:'',
              password:'',
              confirm_password:'',

        }} onSubmit={(v)=>{console.log(v)}}>

          <Form style={{textAlign:'center',padding:'10px'}}>
              <label htmlFor='firstname'>Name:</label>
              <Field type='text' placeholder='enter your name here' id='name' name='firstname' />
              <Error name='firstname' />
              <br /><br />
             

              <label htmlFor='lastname'>Lastname:</label>
              <Field type='text' placeholder='enter surname' id='lastname' name='lastname' />
              <Error name='lastname' />
              <br /><br />


              <label htmlFor='number'>Phone no.</label>
              <Field type='text' placeholder='enter phone no.' id='number' name='number' />
              <Error name='number' />
              <br /><br />
             

                <label htmlFor='address'>Address:</label>
              <textarea id='address' name='address' rows='5' cols='20'></textarea>
              <Error name='address' />
              <br /><br />

              <label htmlFor='email'>Emilid:</label>
              <Field type='text' placeholder='email-id' id='email' name='email' />
              <Error name='email' />
              <br /><br />

              <label htmlFor='password'>Password:</label>
              <Field type='password' placeholder='enter password' id='password' name='password' />
              <Error name='password' />
              <br /><br />

              <label htmlFor='confirm_password'>Confirm Password:</label>
              <Field type='password' placeholder='confirm password' id='confirm_password' name='confirm_password' />
              <Error name='confirm_password' />
              <br /><br />

            <label htmlFor='file'>Select File :</label> 
              <Field type='file' name='file' id='file' accept='image/*' />
              <Error name='file' />
              <br /><br />

              <button type='submit'>Submit</button>
          </Form>
      </Formik>
    </div>
  )
}

export default FormikForm
