import React, { Component } from 'react'

 class StateClassComp extends Component {
     state={
         name:'netra'
     }
      namechange=()=>{
        this.setState({name:'Bob'})
    }
  render() {
   
    return (
      <div>
        <h1>hello { this.state.name}</h1>
        <button onClick={this.namechange}>click to change name</button>
      </div>
    )
  }
}

export default StateClassComp
