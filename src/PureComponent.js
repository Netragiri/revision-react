import React, { PureComponent } from 'react'

class PureComp extends PureComponent {
    constructor(props) {
      super(props)
    
      this.state = {
         count:0
      }
    }
    
  render() {
      console.log(this.state.count)
    return (
      <div>
   <button onClick={()=>{this.setState({count:0})}}>Click me</button>
      </div>
    )
  }
}

export default PureComp
