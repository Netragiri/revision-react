import React, { Component } from 'react'
import ForwardRef from './ForwardRef'

 class ParentForwardref extends Component {
    constructor(props) {
      super(props)
    
      this.inputref=React.createRef()
    }
    clickhandler=()=>{
        this.inputref.current.focus()
        this.inputref.current.style.backgroundColor='blue'
    }
  render() {
    return (
      <div>
        <ForwardRef ref={this.inputref} />
        <button onClick={this.clickhandler}>Click here</button>
      </div>
    )
  }
}

export default ParentForwardref
