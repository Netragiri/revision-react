import React, { useState } from 'react'
import UpdatedComponenet from './Hoc'

function ClickHOC(props) {
    
  return (
    <div>
      <button onClick={props.incrementCount}>clicked  {props.item} timed</button>
    </div>
  )
}

export default UpdatedComponenet(ClickHOC)
