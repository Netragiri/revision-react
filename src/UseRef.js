import React, { useRef } from 'react'

function UseRef() {
    const inputref=useRef()
    const handlechange=()=>{
        inputref.current.style.color='red'
        inputref.current.style.fontSize='20px'
    }
  return (
    <div>
      <h1 ref={inputref}>Demo of Useref</h1>
      <button onClick={handlechange}>Click to change color</button>
    </div>
  )
}

export default UseRef
